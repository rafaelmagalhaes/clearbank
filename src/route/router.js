import Vue from "vue";
import Router from "vue-router";
import Home from "../views/Home.vue";
import AddQuery from "@/views/AddQuery.vue";
Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/add-query",
      name: "AddQuery",
      component: AddQuery
    }
  ]
});
