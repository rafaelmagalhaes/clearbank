import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./route/router";
import store from "./store/store";
import {
  ValidationProvider,
  ValidationObserver
} from "vee-validate/dist/vee-validate.full";

require("./assets/style/main.less");
Vue.config.productionTip = false;

Vue.component("validation-provider", ValidationProvider);
Vue.component("validation-observer", ValidationObserver);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
