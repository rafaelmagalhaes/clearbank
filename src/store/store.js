import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

import route from "@/route/router";
Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [new VuexPersistence().plugin], //save the store.state to localstorage
  state: {
    requests: []
  },
  getters: {
    getRequests: state => state.requests
  },
  mutations: {
    setData(state, payload) {
      state.requests.push(payload);
    }
  },
  actions: {
    saveData({ commit }, payload) {
      commit("setData", payload);
      setTimeout(() => {
        route.push("/");
      });
    }
  }
});
