# clearbank


## Task

1. Initialise VueJS in an online editor (CodeSandbox Vue Template) or using a
Webpack bundle submitted to a public Git repository
2. Create Store / State Management (VueX)
3. Use of a UI framework is optional.
4. Create a summary component which displays form entries from the Store
5. Create a form component (this form should be controlled by an ‘Add’ button and
disappear after successful form submission)
Include fields listed below:
- First name
- Surname
- E-mail address
- Customer query
- Submit button
Make fields listed below required:
- First name
- Surname
- E-mail address
Make fields listed below accept E-MAIL format only (example@company.domain):
- E-mail address
6. On submit, validate the form
7. If the form submits successfully set all the details in the Store
8. Ensure the summary component is updated
9. Save and share with us the link to the editor when you're happy with your work

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
